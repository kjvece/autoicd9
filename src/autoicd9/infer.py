#!/usr/bin/env python


import re
import nltk
import torch
import functools
import numpy as np
from gensim.models import Word2Vec
from pathlib import Path
from multiprocessing import Pool


import argparse
from typing import List, Optional, Any


def levenshtein(a: str, b: str) -> int:
    return nltk.edit_distance(a, b)


def preprocess_text(text: str, data: Path, num_workers: int) -> List[str]:

    dictionary_path = data / "dictionary.txt"

    # replace punctuation with whitespace
    text = re.sub(r"[^\w\s]", " ", text)

    # replace digits with d
    text = re.sub(r"\d+", "d", text)

    # convert to lowercase
    text = text.lower()

    if not dictionary_path.exists():
        raise Exception("ERROR: dictionary not found")

    with open(dictionary_path, "r") as f:
        common_words = [line.strip() for line in f]

    with Pool(num_workers) as p:
        tokens = text.split()
        tokens = process_levenshtein(" ".join(tokens), common_words, p).split()

    return tokens


def process_levenshtein(sentence: str, dictionary: List[str], pool: Optional[Any]=None) -> str:
    tokens = sentence.split()
    for i,t in enumerate(tokens):
        if t not in dictionary:
            if pool is not None:
                distances = pool.map(functools.partial(levenshtein, t), dictionary)
            else:
                distances = list(map(functools.partial(levenshtein, t), dictionary))
            min_distance = min(distances)
            i_min_distance = distances.index(min_distance)
            tokens[i] = dictionary[i_min_distance]
    return " ".join(tokens)


def infer_icd9(args: argparse.Namespace) -> None:

    data = args.data
    text_file: Path = args.text_file
    with (data / "icd9.txt").open() as f:
        icd9_codes = f.readlines()
    i_to_icd = { i:icd for i,icd in enumerate(icd9_codes) }
    w2v = Word2Vec.load(str(data / "word2vec.model"), mmap="r")

    if not text_file.exists() or not text_file.is_file():
        raise Exception(f"ERROR: cannot read file {text_file!r}")

    if not args.model.exists() or not args.model.is_file():
        raise Exception(f"ERROR: cannot read file {text_file!r}")

    with open(text_file, "r") as f:
        text = f.read()

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    text = preprocess_text(text, data, args.workers)
    embedding_dim = 100
    text_tensor = torch.tensor(np.array([w2v.wv[token] if token in w2v.wv else [0]*embedding_dim for token in text]))
    text_tensor = text_tensor.unsqueeze(0)
    text_tensor = text_tensor.to(device)

    model = torch.jit.load(args.model)
    model.eval()
    model.to(device)

    try:
        y_pred = model(text_tensor)
        predictions = (y_pred[0] > 0.5).nonzero().squeeze()
        icd9_predictions = [i_to_icd[prediction.item()] for prediction in predictions]
    except:
        # this is hacky, but because some of the models need the data in a fixed size
        # do the mean conversion upon failure (namely the bot* and mlp models)
        try:
            text_tensor = text_tensor.mean(dim=1)
            y_pred = model(text_tensor)
            predictions = (y_pred[0] > 0.5).nonzero()
            icd9_predictions = [i_to_icd[prediction.item()] for prediction in predictions]
        except:
            icd9_predictions = []
    print("Predicted ICD9 Codes:")
    print("\n".join([idc9.strip() for idc9 in icd9_predictions]))

    
