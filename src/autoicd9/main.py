#!/usr/bin/env python

import argparse
from pathlib import Path


from autoicd9.preprocess import preprocess_csv
from autoicd9.word2vec import process_embeddings
from autoicd9.trainer import train_model
from autoicd9.models import models
from autoicd9.infer import infer_icd9


def main() -> None:
    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(required=True)

    default_output = Path("./processed_data")

    preprocess = subparsers.add_parser("preprocess", help="preprocess dataset")
    preprocess.set_defaults(func=preprocess_csv)
    preprocess.add_argument(
        "-d",
        "--diagnoses",
        action="store",
        type=Path,
        default=Path("./data/DIAGNOSES_ICD.csv"),
        help="path to DIAGNOSES_ICD.csv file from MIMIC III data (default './data/DIAGNOSES_ICD.csv')",
    )
    preprocess.add_argument(
        "-n",
        "--noteevents",
        action="store",
        type=Path,
        default=Path("./data/NOTEEVENTS.csv"),
        help="path to NOTEEVENTS.csv file from MIMIC III data (default './data/NOTEEVENTS.csv')",
    )
    preprocess.add_argument(
        "-o",
        "--output",
        action="store",
        type=Path,
        default=default_output,
        help=f"path to directory that will contain processed data (default {default_output})",
    )
    preprocess.add_argument(
        "-c",
        "--count",
        action="store",
        type=int,
        default=5,
        help="minimum occurence of word to be considered common (default 5)",
    )
    preprocess.add_argument(
        "-w",
        "--workers",
        action="store",
        type=int,
        default=4,
        help="number of workers to use to calculate levenshtein distance (default 4)",
    )
    preprocess.add_argument(
        "-s",
        "--skip-levenshtein",
        action="store_true",
        help="skip levenshtein calculation when processing",
    )

    word2vec = subparsers.add_parser("word2vec", help="process word2vec embeddings for dataset")
    word2vec.set_defaults(func=process_embeddings)
    word2vec.add_argument(
        "-d",
        "--data",
        action="store",
        type=Path,
        default=default_output,
        help="path to preprocessed data directory",
    )
    word2vec.add_argument(
        "-w",
        "--workers",
        action="store",
        type=int,
        default=4,
        help="number of workers to use (default 4)",
    )
    word2vec.add_argument(
        "-f",
        "--force",
        action="store_true",
        help="force creation of new model (ignore existing)",
    )

    trainer = subparsers.add_parser("train", help="train the model on the preprocessed data")
    trainer.set_defaults(func=train_model)
    trainer.add_argument(
        "-d",
        "--data",
        action="store",
        type=Path,
        default=default_output,
        help="path to preprocessed data directory",
    )
    trainer.add_argument(
        "-m",
        "--model",
        action="store",
        default=list(models.keys())[0],
        choices=list(models.keys()),
        help="which model to use",
    )
    trainer.add_argument(
        "-b",
        "--batch_size",
        action="store",
        default=32,
        type=int,
        help="batch size to train with (default 32)",
    )
    trainer.add_argument(
        "-e",
        "--epochs",
        action="store",
        default=5,
        type=int,
        help="max number of epochs to train for (default 5)",
    )
    trainer.add_argument(
        "-f",
        "--folds",
        action="store",
        default=5,
        type=int,
        help="number of folds to train on (default 5)",
    )
    trainer.add_argument(
        "--depth",
        action="store",
        default=1,
        type=int,
        choices=[1,2,3],
        help="number of extra depth layers to add, if applicable to model (default 1)",
    )
    trainer.add_argument(
        "--hidden",
        action="store",
        default=64,
        type=int,
        help="size of the hidden layers, if applicable to model (default 64)",
    )
    trainer.add_argument(
        "--filters",
        action="store",
        default=250,
        type=int,
        help="number of convolutional filters, if applicable to model (default 250)",
    )

    infer = subparsers.add_parser("infer", help="infer ICD9 codes using trained model")
    infer.set_defaults(func=infer_icd9)
    infer.add_argument(
        "-d",
        "--data",
        action="store",
        type=Path,
        default=default_output,
        help="path to preprocessed data directory",
    )
    infer.add_argument(
        "-w",
        "--workers",
        action="store",
        type=int,
        default=4,
        help="number of workers to use (default 4)",
    )
    infer.add_argument(
        "model",
        action="store",
        type=Path,
        help="path to model to load",
    )
    infer.add_argument(
        "text_file",
        action="store",
        type=Path,
        help="path to text file to base inference on",
    )

    args = parser.parse_args()
    args.func(args)


if __name__ == "__main__":
    main()
