#!/usr/bin/env python


import os
import nltk
import shutil
import functools
import pandas as pd
from pathlib import Path
from multiprocessing import Pool


import argparse
from typing import List, Optional, Any


def levenshtein(a: str, b: str) -> int:
    return nltk.edit_distance(a, b)

def preprocess_text(noteevents: pd.DataFrame, output: Path, common_count: int, num_workers: int) -> None:

    save_csv = output / "noteevents.csv"
    dictionary_path = output / "dictionary.txt"

    if not save_csv.exists():

        print(f"Converting all punctuation to whitespace...")
        noteevents["TEXT"] = noteevents["TEXT"].replace(to_replace=r'[^\w\s]', value=" ", regex=True)

        print(f"Converting all numbers to the character 'd'...")
        noteevents["TEXT"] = noteevents["TEXT"].replace(to_replace=r'\d+', value=" d ", regex=True)

        print(f"Converting all letters to lowercase...")
        noteevents["TEXT"] = noteevents["TEXT"].str.lower()

        noteevents.to_csv(save_csv)
        print(f'Saved text to: {save_csv}')

    else:

        print(f'Found precomputed text at: {save_csv}')

    noteevents: pd.DataFrame = pd.read_csv(save_csv)

    if not dictionary_path.exists():

        print(f"Splitting on whitespace...")
        dictionary = {}
        for row in noteevents.itertuples():
            tokens = row.TEXT.split()
            for t in tokens:
                dictionary[t] = dictionary.get(t, 0) + 1
        print(f'Size of raw dictionary: {len(dictionary)}')

        common_count = max(1, common_count)
        print(f"Removing words with occurances less than {common_count}x...")
        common_dictionary = {}
        for word in dictionary:
            count = dictionary[word]
            if count >= common_count:
                common_dictionary[word] = count
        print(f'Size of dictionary with common words only: {len(common_dictionary)}')
        with open(dictionary_path, "w") as f:
            for word in common_dictionary:
                f.write(word + "\n")
        print(f'Saved dictionary to: {dictionary_path}')

    else:

        print(f'Found precomputed dictionary at: {dictionary_path}')

    with open(dictionary_path, "r") as f:
        common_words = [line.strip() for line in f]

    print(f"Replacing low frequency words with closest levenshtein word in dictionary...")
    existing_data = os.listdir(output)

    with Pool(num_workers) as p:

        for row in noteevents.itertuples():
            try:
                hamd_id = int(row.HADM_ID)
            except ValueError:
                continue
            tokens = row.TEXT.split()

            data_dir = Path(output / str(hamd_id))
            if str(hamd_id) in existing_data:
                continue

            data_dir.mkdir(parents=True, exist_ok=True)
            with open(data_dir / "original.txt", "a") as f:
                for t in tokens:
                    f.write(t + " ")

            tokens = process_levenshtein(" ".join(tokens), common_words, p).split()

            with open(data_dir / "text.txt", "a") as f:
                for t in tokens:
                    f.write(t + " ")

def process_levenshtein(sentence: str, dictionary: List[str], pool: Optional[Any]=None) -> str:
    tokens = sentence.split()
    for i,t in enumerate(tokens):
        if t not in dictionary:
            if pool is not None:
                distances = pool.map(functools.partial(levenshtein, t), dictionary)
            else:
                distances = list(map(functools.partial(levenshtein, t), dictionary))
            min_distance = min(distances)
            i_min_distance = distances.index(min_distance)
            tokens[i] = dictionary[i_min_distance]
    return " ".join(tokens)


def preprocess_csv(args: argparse.Namespace) -> None:
    if not args.diagnoses.exists():
        raise Exception(f"File '{args.diagnoses}' does not exist")
    if not args.noteevents.exists():
        raise Exception(f"File '{args.noteevents}' does not exist")

    output = args.output
    output.mkdir(parents=True, exist_ok=True)

    print(f"Loading data...")
    diagnoses: pd.DataFrame = pd.read_csv(args.diagnoses)
    noteevents: pd.DataFrame = pd.read_csv(args.noteevents)

    diagnoses = diagnoses.filter(["HADM_ID", "ICD9_CODE"])
    noteevents = noteevents.filter(["HADM_ID", "TEXT"])

    print(f'Total number of notes: {noteevents["HADM_ID"].shape[0]}')
    print(f'Number of unique visits in notes: {pd.unique(noteevents["HADM_ID"]).shape[0]}')
    print(f'Total number of diagnoses: {diagnoses["HADM_ID"].shape[0]}')
    print(f'Number of unique visits in diagnoses: {pd.unique(diagnoses["HADM_ID"]).shape[0]}')

    print(f"Processing data...")

    if not args.skip_levenshtein:
        preprocess_text(noteevents, output, args.count, args.workers)

    print(f"Writing corresponding icd9 codes...")
    all_icds = set()
    for row in noteevents.itertuples():
        try:
            hamd_id = int(row.HADM_ID)
        except ValueError:
            continue
        data_dir = Path(output / str(hamd_id))
        if not data_dir.exists():
            continue
        text_data = data_dir / "text.txt"
        if not text_data.exists():
            shutil.rmtree(data_dir)
            continue
        icds = diagnoses.loc[diagnoses["HADM_ID"] == hamd_id]
        if icds.size <= 0:
            shutil.rmtree(data_dir)
            continue
        with open(data_dir / "icd9.txt", "w") as f:
            for icd in icds.itertuples():
                f.write(icd.ICD9_CODE + "\n")
                all_icds.add(icd.ICD9_CODE)

    print(f"Writing icd9 code map...")
    with open(output / "icd9.txt", "w") as f:
        for icd in all_icds:
            f.write(icd + "\n")
    print(f"Total unique ICD9 codes: {len(all_icds)}")

    print(f"Finished! Preprocessed data located in '{output}'")

