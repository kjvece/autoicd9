#!/usr/bin/env python

import math
import torch
import pathlib
from datetime import datetime
import numpy as np
from torch.utils.data import Dataset, DataLoader, SubsetRandomSampler
from torch.utils.tensorboard.writer import SummaryWriter
from sklearn.model_selection import KFold
from ignite.engine import Engine, Events
from ignite.metrics import Loss
from ignite.metrics.precision import Precision
from ignite.metrics.recall import Recall
from ignite.handlers import EarlyStopping


from autoicd9.models import models
from autoicd9.dataset import MIMICSet


import argparse
from typing import List, Tuple, Any


def setup_dataflow(
        dataset: Dataset,
        batch_size: int,
        collate_fn,
        train_idx: List[int],
        val_idx: List[int]
    ) -> Tuple[DataLoader, DataLoader]:
    train_sampler = SubsetRandomSampler(train_idx)
    val_sampler = SubsetRandomSampler(val_idx)

    train_loader = DataLoader(dataset, batch_size=batch_size, collate_fn=collate_fn, sampler=train_sampler)
    val_loader = DataLoader(dataset, batch_size=batch_size, collate_fn=collate_fn, sampler=val_sampler)

    return train_loader, val_loader


def create_model(args: argparse.Namespace, output_size: int, device: torch.device) -> Tuple[torch.nn.Module, Any, Any]:
    model: torch.nn.Module = models[args.model].model(
        output_size=output_size,
        depth=args.depth,
        hidden_size=args.hidden,
        num_filters=args.filters,
    )

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = model.to(device)

    optimizer = torch.optim.Adam(params=model.parameters())
    criterion = torch.nn.BCELoss()

    return model, optimizer, criterion
    

def train_model(args: argparse.Namespace) -> None:

    MAX_EPOCHS = args.epochs
    NUM_FOLDS = args.folds
    collate_fn = models[args.model].collate_fn

    with SummaryWriter() as writer:

        main_tag = f"{datetime.now().isoformat()}/{args.model}_"

        dataset = MIMICSet(args.data)
        train_loader, _ = setup_dataflow(
            dataset,
            batch_size=args.batch_size,
            collate_fn=collate_fn,
            train_idx=[0],
            val_idx=[1],
        )
        _,y = next(iter(train_loader))
        output_size = y.shape[1]
        print(f"Output shape: {output_size}")

        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

        def run_training(
                fold_i: int,
                train_data: DataLoader,
                val_data: DataLoader,
            ) -> Tuple[torch.nn.Module, List[float], List[float], List[float], List[float]]:

            model, optimizer, criterion = create_model(args, output_size, device)

            final_precision = []
            final_recall = []
            final_f1 = []
            final_loss = []

            def train_step(_, batch):
                model.train()
                x,y = batch
                x,y = x.to(device), y.to(device)

                y_hat = model(x)
                y_pred = torch.round(y_hat)
                loss = criterion(y_hat, y)

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

                return {
                    "y": y.detach().cpu(),
                    "y_pred": y_pred.detach().cpu(),
                    "loss": loss.item(),
                    "criterion_kwargs": {},
                }

            trainer = Engine(train_step)

            precision = Precision()
            recall = Recall()
            val_metrics = {
                "precision": precision,
                "recall": recall,
                "f1": (precision * recall * 2 / (precision + recall)).mean(),
                "loss": Loss(criterion),
            }
            for name, metric in val_metrics.items():
                metric.attach(trainer, name)

            @trainer.on(Events.STARTED)
            def log_start(trainer):
                print(f"===== FOLD {fold_i} =====")

            @trainer.on(Events.EPOCH_COMPLETED)
            def log_train_results(trainer):
                metrics = trainer.state.metrics
                writer.add_scalar(
                    main_tag + f"train_fold_{fold_i}_loss",
                    metrics["loss"],
                    trainer.state.epoch,
                )
                writer.add_scalars(
                    main_tag + f"train_fold_{fold_i}",
                    {
                        "precision": metrics["precision"],
                        "recall": metrics["recall"],
                        "f1": metrics["f1"],
                    },
                    trainer.state.epoch,
                )
                print(
                    f"Fold [{fold_i}] - Epoch [{trainer.state.epoch}/{MAX_EPOCHS}] TRAIN - "
                    f"loss({metrics['loss']:.4f}), "
                    f"precision({metrics['precision']*100:.1f}), "
                    f"recall({metrics['recall']*100:.1f}), "
                    f"f1({metrics['f1']*100:.1f})"
                )

            def val_step(_, batch):
                model.eval()
                x,y = batch
                x,y = x.to(device), y.to(device)

                with torch.no_grad():
                    y_hat = model(x)
                    y_pred = torch.round(y_hat)
                    loss = criterion(y_hat, y)

                return {
                    "y": y.detach().cpu(),
                    "y_pred": y_pred.detach().cpu(),
                    "loss": loss.item(),
                    "criterion_kwargs": {},
                }
            evaluator = Engine(val_step)
            for name, metric in val_metrics.items():
                metric.attach(evaluator, name)

            def score_function(engine):
                loss = engine.state.metrics["loss"]
                return -loss
            handler = EarlyStopping(patience=2, score_function=score_function, trainer=trainer)
            evaluator.add_event_handler(Events.COMPLETED, handler)

            @trainer.on(Events.EPOCH_COMPLETED)
            def log_val_results(trainer):
                evaluator.run(val_data)
                metrics = evaluator.state.metrics
                final_loss.append(metrics["loss"])
                final_precision.append(metrics["precision"])
                final_recall.append(metrics["recall"])
                final_f1.append(metrics["f1"])
                writer.add_scalar(
                    main_tag + f"validation_fold_{fold_i}_loss",
                    metrics["loss"],
                    trainer.state.epoch,
                )
                writer.add_scalars(
                    main_tag + f"validation_fold_{fold_i}",
                    {
                        "precision": metrics["precision"],
                        "recall": metrics["recall"],
                        "f1": metrics["f1"],
                    },
                    trainer.state.epoch,
                )
                print(
                    f"Fold [{fold_i}] - Epoch [{trainer.state.epoch}/{MAX_EPOCHS}] VAL - "
                    f"loss({metrics['loss']:.4f}), "
                    f"precision({metrics['precision']*100:.1f}), "
                    f"recall({metrics['recall']*100:.1f}), "
                    f"f1({metrics['f1']*100:.1f})"
                )

            trainer.run(train_data, max_epochs=MAX_EPOCHS)

            return (
                model,
                final_precision,
                final_recall,
                final_f1,
                final_loss,
            )

        splits = KFold(n_splits=NUM_FOLDS, shuffle=True, random_state=1337)

        precisions = []
        recalls = []
        f1s = []
        losses = []
        lowest_loss: float = 99999999
        best_model = None

        for fold_i, (train_idx, val_idx) in enumerate(splits.split(np.arange(len(dataset)))):
            train_loader, val_loader = setup_dataflow(
                dataset,
                batch_size=args.batch_size,
                collate_fn=collate_fn,
                train_idx=train_idx,
                val_idx=val_idx,
            )

            model, pr, re, f1, loss = run_training(fold_i+1, train_loader, val_loader)
            if best_model is None or loss[-1] < lowest_loss:
                best_model = model
            precisions.append(pr)
            recalls.append(re)
            f1s.append(f1)
            losses.append(loss)

        output_dir = pathlib.Path("models")
        output_dir.mkdir(parents=True, exist_ok=True)
        model_jit = torch.jit.script(model)
        save_base = args.model
        if args.model.endswith("ndense"):
            save_base += f"-{args.depth}dense"
            if args.hidden != 64:
                save_base += f"-{args.hidden}hidden"
        if args.filters != 250:
            save_base += f"-{args.filters}filters"
        model_jit.save(output_dir / f"{save_base}.pt")

        def pad(l: List[float], width: int) -> List[float]:
            extra = width - len(l)
            if extra > 0:
                l.extend([l[-1]] * (extra))
            return list(map(lambda x: x if not math.isnan(x) else 0.0, l))

        avg_precision = np.mean(precisions[-1])
        avg_recall = np.mean(recalls[-1])
        avg_f1 = np.mean(f1s[-1])
        avg_loss = np.mean(losses[-1])
        print("===== FINAL RESULTS =====")
        print(f"Average Precision: {avg_precision*100:.1f}")
        print(f"Average Recall: {avg_recall*100:.1f}")
        print(f"Average F1: {avg_f1*100:.1f}")
        print(f"Average Loss: {avg_loss:.4f}")

        longest = max([len(l) for l in precisions])
        precisions = [pad(pr, longest) for pr in precisions]
        for i,pr in enumerate(zip(*precisions)):
            avg_pr = np.mean(pr)
            writer.add_scalar(main_tag + "avg_precision", avg_pr, i)
        recalls = [pad(pr, longest) for pr in recalls]
        for i,re in enumerate(zip(*recalls)):
            avg_re = np.mean(re)
            writer.add_scalar(main_tag + "avg_recall", avg_re, i)
        f1s = [pad(pr, longest) for pr in f1s]
        for i,f1 in enumerate(zip(*f1s)):
            avg_f1 = np.mean(f1)
            writer.add_scalar(main_tag + "avg_f1", avg_f1, i)
        losses = [pad(pr, longest) for pr in f1s]
        for i,loss in enumerate(zip(*losses)):
            avg_loss = np.mean(loss)
            writer.add_scalar(main_tag + "avg_loss", avg_loss, i)
