#!/usr/bin/env python

import torch
import numpy as np
from torch.utils.data import Dataset
from gensim.models import Word2Vec

from pathlib import Path
from typing import List, Tuple, Union

class MIMICSet(Dataset):
    
    def __init__(self, basepath: Path) -> None:
        self.path = basepath
        self.data = [ hamd_id for hamd_id in basepath.iterdir() if hamd_id.is_dir() ]
        with (basepath / "icd9.txt").open() as f:
            lines = f.readlines()
        self.icd_to_i = { icd:i for i,icd in enumerate(lines) }
        self.i_to_icd = { i:icd for i,icd in enumerate(lines) }
        self.num_icds = len(self.icd_to_i)
        self.model = Word2Vec.load(str(basepath / "word2vec.model"), mmap="r")

    def to_icd(self, i: int) -> Union[str, None]:
        return self.i_to_icd.get(i)

    def from_icd(self, icd: str) -> Union[int, None]:
        return self.icd_to_i.get(icd)

    def __len__(self) -> int:
        return len(self.data)

    def __getitem__(self, index: int) -> Tuple[torch.Tensor, torch.Tensor]:
        x_path = self.data[index]
        f_text = x_path / "text.txt"
        f_icd = x_path / "icd9.txt"
        with f_text.open() as f:
            text = f.read().split()
        text_tensors = torch.tensor(np.array([self.model.wv[token] for token in text]))

        with f_icd.open() as f:
            icds = f.readlines()
        icd_label = torch.zeros(self.num_icds, dtype=torch.float)
        icd_i = [ self.from_icd(icd) for icd in icds ]
        icd_label[icd_i] = 1
        return text_tensors, icd_label
