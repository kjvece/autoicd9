#!/usr/bin/env python

import os
from gensim.models import Word2Vec

import argparse
from pathlib import Path


def process_embeddings(args: argparse.Namespace) -> None:
    data: Path = args.data
    num_workers: int = args.workers

    model_path = data / "word2vec.model"

    if args.force or not model_path.exists():
        texts = [ hamd_id / "text.txt" for hamd_id in data.iterdir() if hamd_id.is_dir() ]
        sentences = []
        for text in texts:
            with text.open() as f:
                sentences.append(f.read().split())

        print(f"creating model...")
        model = Word2Vec(sentences=sentences, vector_size=100, window=5, min_count=1, workers=num_workers)
        model.save(str(model_path))
        print(f"saved word2vec model found at: {model_path}")
    else:
        print(f"using existing model found at: {model_path}")

