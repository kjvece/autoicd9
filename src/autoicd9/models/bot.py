#!/usr/bin/env python

import torch


from typing import Tuple, List

def collate_fn(batch: List[Tuple[torch.Tensor, torch.Tensor]]) -> Tuple[torch.Tensor, torch.Tensor]:
    text_tensors, icd_labels = zip(*batch)
    text_tensor = torch.stack([tensor.mean(dim=0) for tensor in text_tensors])
    icd_tensor = torch.stack(icd_labels)
    return text_tensor, icd_tensor

class BOT(torch.nn.Module):

    def __init__(self, embedding_dim: int=100, output_size: int=100, **_):
        super().__init__()
        self.fc = torch.nn.Linear(embedding_dim, output_size)

    def forward(self, x: torch.Tensor):
        x = self.fc(x)
        x = torch.sigmoid(x)
        return x

model = BOT
