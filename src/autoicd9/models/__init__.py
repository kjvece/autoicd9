#!/usr/bin/env python

from os.path import dirname, basename, isfile, join
import glob
import importlib

modules = glob.glob(join(dirname(__file__), "*.py"))
models = {
    basename(f)[:-3]: importlib.import_module("." + basename(f)[:-3], __name__)
    for f in modules if isfile(f) and not f.endswith("__init__.py")
}
