#!/usr/bin/env python


import torch
from torch.nn import functional as F


from typing import Tuple, List


def collate_fn(batch: List[Tuple[torch.Tensor, torch.Tensor]]) -> Tuple[torch.Tensor, torch.Tensor]:
    text_tensors, icd_labels = zip(*batch)
    text_tensor = torch.nn.utils.rnn.pad_sequence(text_tensors, batch_first=True)
    icd_tensor = torch.stack(icd_labels)
    return text_tensor, icd_tensor


class CNN_NDense(torch.nn.Module):

    def __init__(
        self,
        embedding_dim: int=100,
        num_filters: int=250,
        hidden_size: int=64,
        output_size: int=100,
        depth: int=1,
        **_
    ):
        super().__init__()
        self.conv = torch.nn.Conv1d(
            in_channels=embedding_dim,
            out_channels=num_filters,
            kernel_size=3,
        )
        self.layers = torch.nn.Sequential(
            torch.nn.Linear(num_filters, hidden_size),
            torch.nn.ReLU(),
        )
        for _ in range(depth-1):
            self.layers.append(torch.nn.Linear(hidden_size, hidden_size))
            self.layers.append(torch.nn.ReLU())
        self.layers.append(torch.nn.Linear(hidden_size, output_size))

    def forward(self, x: torch.Tensor):
        x = torch.swapaxes(x, 1, 2)
        x = F.relu(self.conv(x))
        x,_ = torch.max(x, -1) 
        x = self.layers(x)
        x = torch.sigmoid(x)
        return x


model = CNN_NDense
