#!/usr/bin/env python


import torch
from torch.nn import functional as F


from typing import Tuple, List


def collate_fn(batch: List[Tuple[torch.Tensor, torch.Tensor]]) -> Tuple[torch.Tensor, torch.Tensor]:
    text_tensors, icd_labels = zip(*batch)
    text_tensor = torch.nn.utils.rnn.pad_sequence(text_tensors, batch_first=True)
    icd_tensor = torch.stack(icd_labels)
    return text_tensor, icd_tensor


class CNN(torch.nn.Module):

    def __init__(self, embedding_dim: int=100, num_filters: int=250, output_size: int=100, **_):
        super().__init__()
        self.conv = torch.nn.Conv1d(
            in_channels=embedding_dim,
            out_channels=num_filters,
            kernel_size=3,
        )
        self.fc = torch.nn.Linear(num_filters, output_size)
            

    def forward(self, x: torch.Tensor):
        x = torch.swapaxes(x, 1, 2)
        x = F.relu(self.conv(x))
        x,_ = torch.max(x, -1) 
        x = self.fc(x)
        x = torch.sigmoid(x)
        return x


model = CNN
