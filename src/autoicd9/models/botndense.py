#!/usr/bin/env python

import torch
from torch.nn import functional as F


from typing import Tuple, List

def collate_fn(batch: List[Tuple[torch.Tensor, torch.Tensor]]) -> Tuple[torch.Tensor, torch.Tensor]:
    text_tensors, icd_labels = zip(*batch)
    text_tensor = torch.stack([tensor.mean(dim=0) for tensor in text_tensors])
    icd_tensor = torch.stack(icd_labels)
    return text_tensor, icd_tensor

class BOTNDense(torch.nn.Module):

    def __init__(
        self,
        embedding_dim: int=100,
        hidden_size: int=64,
        output_size: int=100,
        depth: int=1,
        **_
    ):
        super().__init__()

        self.layers = torch.nn.Sequential(
            torch.nn.Linear(embedding_dim, hidden_size),
            torch.nn.ReLU(),
        )
        for _ in range(depth-1):
            self.layers.append(torch.nn.Linear(hidden_size, hidden_size))
            self.layers.append(torch.nn.ReLU())
        self.layers.append(torch.nn.Linear(hidden_size, output_size))

    def forward(self, x: torch.Tensor):
        x = self.layers(x)
        x = torch.sigmoid(x)
        return x

model = BOTNDense
