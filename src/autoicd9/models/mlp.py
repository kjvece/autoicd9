#!/usr/bin/env python

import torch


from typing import Tuple, List

def collate_fn(batch: List[Tuple[torch.Tensor, torch.Tensor]]) -> Tuple[torch.Tensor, torch.Tensor]:
    text_tensors, icd_labels = zip(*batch)
    text_tensor = torch.stack([tensor.mean(dim=0) for tensor in text_tensors])
    icd_tensor = torch.stack(icd_labels)
    return text_tensor, icd_tensor

class MLP(torch.nn.Module):

    def __init__(self, embedding_dim: int=100, output_size: int=100, **_):
        super().__init__()
        self.fc1 = torch.nn.Linear(embedding_dim, output_size)
        self.fc2 = torch.nn.Linear(output_size, output_size)

    def forward(self, x: torch.Tensor):
        x = self.fc1(x)
        x = torch.relu(x)
        x = self.fc2(x)
        x = torch.sigmoid(x)
        return x

model = MLP
