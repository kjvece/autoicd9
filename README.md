# Automatic ICD9 Medical Coding

An implementation and verification of the ICD9-CM medical coding model presented by V. Pereria et. al. in "Automated ICD-9-CM medical coding of diabetic patient's clinical reports", available at [https://dl.acm.org/doi/10.1145/3279996.3280019](https://dl.acm.org/doi/10.1145/3279996.3280019).

## Results

Results can be viewed by opening tensorboard on the `runs/` directory. The corresponding models are saved in the `models/` directory can be be used with the `infer` step if the 3 files from the `processed_data/` directory are also downloaded. See detailed instructions and help below.

## Instructions

### Prerequisites

* This code requires access to the MIMIC-III dataset. Specifically, the `NOTEEVENTS.csv` and `DIAGNOSIS_ICD.csv` files.
* This code requires python 3.8 or greater
* This code recommends the use of python virtual environments

### Setting Up Environment

It is first recommended to work withing a python virtual environment. If using bash, create and activate one using the following commands:
```
python3 -m venv venv
source venv/bin/activate
```

### Installing the code

This code is distributed as an installable python module. You can use whichever python package manager desired.

For example, the instructions for pip are as follows:
```
pip install git+https://gitlab.com/kjvece/autoicd9.git
```

If using CUDA11.0 and pytorch is not already installed, make sure you use the up to date repositories from pytorch by supplying the package repository index:
```
pip install git+https://gitlab.com/kjvece/autoicd9.git --extra-index-url https://download.pytorch.org/whl/cu113
```

Upon installation, the `autoicd9` command is added to the system $PATH

### Preprocessing the data

The dataset first needs to be preprocessed as outlined in the paper. This includes the following steps:
1) Replacing all punctuation with whitespace
2) Replacing all numbers with the character `d`
3) Tokenizing on whitespace
4) Removing all tokens that do not appear more than `count` times (default 5, as per paper)
5) Replacing all removed tokens with the token that minimizes levenshtein distance from the remaining vocabulary

Preprocess the data using the following command, the data CSV files should be located in the `./data` directory, or supply the necessary parameters with the command:
```
autoicd9 preprocess
```

For a full list of options, use the `-h`/`--help` flag:
```
usage: autoicd9 preprocess [-h] [-d DIAGNOSES] [-n NOTEEVENTS] [-o OUTPUT] [-c COUNT] [-w WORKERS] [-s]

optional arguments:
  -h, --help            show this help message and exit
  -d DIAGNOSES, --diagnoses DIAGNOSES
                        path to DIAGNOSES_ICD.csv file from MIMIC III data (default './data/DIAGNOSES_ICD.csv')
  -n NOTEEVENTS, --noteevents NOTEEVENTS
                        path to NOTEEVENTS.csv file from MIMIC III data (default './data/NOTEEVENTS.csv')
  -o OUTPUT, --output OUTPUT
                        path to directory that will contain processed data (default processed_data)
  -c COUNT, --count COUNT
                        minimum occurence of word to be considered common (default 5)
  -w WORKERS, --workers WORKERS
                        number of workers to use to calculate levenshtein distance (default 4)
  -s, --skip-levenshtein
                        skip levenshtein calculation when processing
```

This command will create a new directory with the processed data. This can be overridden using the `-o`/`--output` option.

Because this step will take a lot of time, you can pause and resume at any time by using `<CTRL-C>` to stop and resuming by reissuing the same command used to start the preprocessing.

If you only want to process a portion of the data, you can use `<CTRL-C>` to stop processsing, then issue the following command to finalize the data
```
autoicd9 preprocess -s
```

### Calculating Word Embeddings

After the data is preprocessed, the word2vec model is used to calculate word embeddings:
```
autoicd9 word2vec
```

Use `-h`/`--help` to get the full options list.
```
usage: autoicd9 word2vec [-h] [-d DATA] [-w WORKERS] [-f]

optional arguments:
  -h, --help            show this help message and exit
  -d DATA, --data DATA  path to preprocessed data directory
  -w WORKERS, --workers WORKERS
                        number of workers to use (default 4)
  -f, --force           force creation of new model (ignore existing)
```

### Training a Model

Assuming the data has been preprocessed and embeddings created, you are ready to train one of the models.

To train a model, use the `train` subcommand, and specify which model with the `-m`/`--model` flag.

For example, to train the BagOfTricks model for a maximum of 25 epochs, use the following command. All models have early stopping with a patience of 2 by default.
```
autoicd9 train -m bot -e 25
```

Use `-h`/`--help` for the full list of options:
```
usage: autoicd9 train [-h] [-d DATA]
                      [-m {multiconv,bot,botndense,convndense,conv,parallelconv,parallelconvndense,mlp}]
                      [-b BATCH_SIZE] [-e EPOCHS] [-f FOLDS] [--depth {1,2,3}]
                      [--hidden HIDDEN] [--filters FILTERS]

options:
  -h, --help            show this help message and exit
  -d DATA, --data DATA  path to preprocessed data directory
  -m {multiconv,bot,botndense,convndense,conv,parallelconv,parallelconvndense,mlp}, --model {multiconv,bot,botndense,convndense,conv,parallelconv,parallelconvndense,mlp}
                        which model to use
  -b BATCH_SIZE, --batch_size BATCH_SIZE
                        batch size to train with (default 32)
  -e EPOCHS, --epochs EPOCHS
                        max number of epochs to train for (default 5)
  -f FOLDS, --folds FOLDS
                        number of folds to train on (default 5)
  --depth {1,2,3}       number of extra depth layers to add, if applicable to
                        model (default 1)
  --hidden HIDDEN       size of the hidden layers, if applicable to model
                        (default 64)
  --filters FILTERS     number of convolutional filters, if applicable to
                        model (default 250)
```

### Model Inference

To predict the ICD9-CM codes for a given file, use the `infer` subcommand.

The command takes the path to a `model.pt` file and a text file for processing. The program needs access to the preprocessed data directory, and will use its metadata to correctly preprocess the new text file.

```
autoicd9 infer path/to/model.pt path/to/text.txt
```

Use `-h`/`--help` for the full list of options:
```
usage: autoicd9 infer [-h] [-d DATA] [-w WORKERS] model text_file

positional arguments:
  model                 path to model to load
  text_file             path to text file to base inference on

options:
  -h, --help            show this help message and exit
  -d DATA, --data DATA  path to preprocessed data directory
  -w WORKERS, --workers WORKERS
                        number of workers to use (default 4)
```


### Available models

#### BagOfTricks `bot`

The BagOfTricks model follows the following architecture:

- Average all input embeddings
- Dense Layer
- Sigmoid Layer

##### Precision
![Graph of BagOfTricks Precision](/images/bot_pr.png "BagOfTricks Precision")

##### Recall
![Graph of BagOfTricks Recall](/images/bot_re.png "BagOfTricks Recall")

##### F1-Score
![Graph of BagOfTricks F1-Score](/images/bot_f1.png "BagOfTricks F1-Score")

#### MultiLayerPerception `mlp`

The MultiLayerPerception model follows the following architecture:

- Average all input embeddings
- Dense Layer with hidden size equal to output size
- Rectified Linear Unit
- Dense Layer
- Sigmoid Layer

##### Precision
![Graph of MultiLayerPerception Precision](/images/mlp_pr.png "MultiLayerPerception Precision")

##### Recall
![Graph of MultiLayerPerception Recall](/images/mlp_re.png "MultiLayerPerception Recall")

##### F1-Score
![Graph of MultiLayerPerception F1-Score](/images/mlp_f1.png "MultiLayerPerception F1-Score")


#### Convolutional `conv`

The Convolutional model follows the following architecture:

- 1-D Convolutional Layer with kernel size 3
- Global Max Pooling
- Dense Layer
- Sigmoid Layer

##### Precision
![Graph of Convolutional Precision](/images/conv_pr.png "Convolutional Precision")

##### Recall
![Graph of Convolutional Recall](/images/conv_re.png "Convolutional Recall")

##### F1-Score
![Graph of Convolutional F1-Score](/images/conv_f1.png "Convolutional F1-Score")

#### Parallel Convolutional `parallelconv`

The Parallel Convolutional model follows the following architecture:

- 3x 1-D Convolutional Layer with kernel sizes of 2, 3, and 4
- Global Max Pooling
- Concatenate all 3 layers
- Dense Layer
- Sigmoid Layer

##### Precision
![Graph of Parallel Convolutional Precision](/images/parallel_pr.png "Parallel Convolutional Precision")

##### Recall
![Graph of Parallel Convolutional Recall](/images/parallel_re.png "Parallel Convolutional Recall")

##### F1-Score
![Graph of Parallel Convolutional F1-Score](/images/parallel_f1.png "Parallel Convolutional F1-Score")

#### N-Dense layers

Each model has an additional `ndense` variant which takes the following additional hyperparameters:

`--hidden` the size of the hidden layers
`--depth` the number of extra dense layers

The ndense models add extra hidden layers before outputting the final model prediction.
